
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.fossilfinders.init;

import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.api.distmarker.Dist;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.CreativeModeTab;

public class FossilFindersModTabs {
	public static CreativeModeTab TAB_DESERT_FOSSILS_MOD;

	public static void load() {
		TAB_DESERT_FOSSILS_MOD = new CreativeModeTab("tabdesert_fossils_mod") {
			@Override
			public ItemStack makeIcon() {
				return new ItemStack(FossilFindersModBlocks.DESERT_FOSSIL.get());
			}

			@OnlyIn(Dist.CLIENT)
			public boolean hasSearchBar() {
				return false;
			}
		};
	}
}
