
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.fossilfinders.init;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.api.distmarker.Dist;

import net.mcreator.fossilfinders.client.renderer.DinoPetRenderer;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class FossilFindersModEntityRenderers {
	@SubscribeEvent
	public static void registerEntityRenderers(EntityRenderersEvent.RegisterRenderers event) {
		event.registerEntityRenderer(FossilFindersModEntities.DINO_PET.get(), DinoPetRenderer::new);
	}
}
