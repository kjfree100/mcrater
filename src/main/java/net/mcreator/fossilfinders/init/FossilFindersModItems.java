
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.fossilfinders.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.BlockItem;

import net.mcreator.fossilfinders.item.SturdyFossilItem;
import net.mcreator.fossilfinders.item.FossilSwordItem;
import net.mcreator.fossilfinders.item.FossilShovelItem;
import net.mcreator.fossilfinders.item.FossilPickaxeItem;
import net.mcreator.fossilfinders.item.FossilHoeItem;
import net.mcreator.fossilfinders.item.FossilAxeItem;
import net.mcreator.fossilfinders.item.FossilArmorItem;
import net.mcreator.fossilfinders.item.DinoPetMakerItem;
import net.mcreator.fossilfinders.item.AncientCharmItem;
import net.mcreator.fossilfinders.FossilFindersMod;

public class FossilFindersModItems {
	public static final DeferredRegister<Item> REGISTRY = DeferredRegister.create(ForgeRegistries.ITEMS, FossilFindersMod.MODID);
	public static final RegistryObject<Item> DESERT_FOSSIL = block(FossilFindersModBlocks.DESERT_FOSSIL, FossilFindersModTabs.TAB_DESERT_FOSSILS_MOD);
	public static final RegistryObject<Item> STURDY_FOSSIL = REGISTRY.register("sturdy_fossil", () -> new SturdyFossilItem());
	public static final RegistryObject<Item> FOSSIL_PICKAXE = REGISTRY.register("fossil_pickaxe", () -> new FossilPickaxeItem());
	public static final RegistryObject<Item> FOSSIL_AXE = REGISTRY.register("fossil_axe", () -> new FossilAxeItem());
	public static final RegistryObject<Item> FOSSIL_SWORD = REGISTRY.register("fossil_sword", () -> new FossilSwordItem());
	public static final RegistryObject<Item> FOSSIL_SHOVEL = REGISTRY.register("fossil_shovel", () -> new FossilShovelItem());
	public static final RegistryObject<Item> FOSSIL_HOE = REGISTRY.register("fossil_hoe", () -> new FossilHoeItem());
	public static final RegistryObject<Item> FOSSIL_ARMOR_HELMET = REGISTRY.register("fossil_armor_helmet", () -> new FossilArmorItem.Helmet());
	public static final RegistryObject<Item> FOSSIL_ARMOR_CHESTPLATE = REGISTRY.register("fossil_armor_chestplate",
			() -> new FossilArmorItem.Chestplate());
	public static final RegistryObject<Item> FOSSIL_ARMOR_LEGGINGS = REGISTRY.register("fossil_armor_leggings", () -> new FossilArmorItem.Leggings());
	public static final RegistryObject<Item> FOSSIL_ARMOR_BOOTS = REGISTRY.register("fossil_armor_boots", () -> new FossilArmorItem.Boots());
	public static final RegistryObject<Item> DINO_PET_MAKER = REGISTRY.register("dino_pet_maker", () -> new DinoPetMakerItem());
	public static final RegistryObject<Item> DIRT_FOSSIL = block(FossilFindersModBlocks.DIRT_FOSSIL, FossilFindersModTabs.TAB_DESERT_FOSSILS_MOD);
	public static final RegistryObject<Item> ANCIENT_CHARM = REGISTRY.register("ancient_charm", () -> new AncientCharmItem());

	private static RegistryObject<Item> block(RegistryObject<Block> block, CreativeModeTab tab) {
		return REGISTRY.register(block.getId().getPath(), () -> new BlockItem(block.get(), new Item.Properties().tab(tab)));
	}
}
