
/*
 *    MCreator note: This file will be REGENERATED on each build.
 */
package net.mcreator.fossilfinders.init;

import net.minecraftforge.registries.RegistryObject;
import net.minecraftforge.registries.ForgeRegistries;
import net.minecraftforge.registries.DeferredRegister;

import net.minecraft.world.level.block.Block;

import net.mcreator.fossilfinders.block.DirtFossilBlock;
import net.mcreator.fossilfinders.block.DesertFossilBlock;
import net.mcreator.fossilfinders.FossilFindersMod;

public class FossilFindersModBlocks {
	public static final DeferredRegister<Block> REGISTRY = DeferredRegister.create(ForgeRegistries.BLOCKS, FossilFindersMod.MODID);
	public static final RegistryObject<Block> DESERT_FOSSIL = REGISTRY.register("desert_fossil", () -> new DesertFossilBlock());
	public static final RegistryObject<Block> DIRT_FOSSIL = REGISTRY.register("dirt_fossil", () -> new DirtFossilBlock());
}
