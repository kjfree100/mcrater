
/*
*    MCreator note: This file will be REGENERATED on each build.
*/
package net.mcreator.fossilfinders.init;

import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.village.WandererTradesEvent;
import net.minecraftforge.event.village.VillagerTradesEvent;
import net.minecraftforge.common.BasicItemListing;

import net.minecraft.world.item.Items;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.entity.npc.VillagerProfession;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.FORGE)
public class FossilFindersModTrades {
	@SubscribeEvent
	public static void registerWanderingTrades(WandererTradesEvent event) {
		event.getGenericTrades().add(new BasicItemListing(new ItemStack(FossilFindersModBlocks.DESERT_FOSSIL.get()),

				new ItemStack(Items.EMERALD, 4), 10, 5, 0.05f));
		event.getGenericTrades().add(new BasicItemListing(new ItemStack(FossilFindersModBlocks.DESERT_FOSSIL.get()),

				new ItemStack(Items.SKELETON_HORSE_SPAWN_EGG), 10, 5, 0.05f));
		event.getGenericTrades().add(new BasicItemListing(new ItemStack(FossilFindersModBlocks.DESERT_FOSSIL.get()),

				new ItemStack(FossilFindersModItems.DINO_PET_MAKER.get()), 10, 5, 0.05f));
	}

	@SubscribeEvent
	public static void registerTrades(VillagerTradesEvent event) {
		if (event.getType() == VillagerProfession.ARMORER) {
			event.getTrades().get(2).add(new BasicItemListing(new ItemStack(FossilFindersModBlocks.DESERT_FOSSIL.get()), new ItemStack(Items.EMERALD),
					new ItemStack(FossilFindersModItems.FOSSIL_ARMOR_CHESTPLATE.get()), 10, 5, 0.05f));
			event.getTrades().get(2).add(new BasicItemListing(new ItemStack(FossilFindersModBlocks.DESERT_FOSSIL.get()), new ItemStack(Items.EMERALD),
					new ItemStack(FossilFindersModItems.FOSSIL_ARMOR_HELMET.get()), 10, 5, 0.05f));
			event.getTrades().get(2).add(new BasicItemListing(new ItemStack(FossilFindersModBlocks.DESERT_FOSSIL.get()), new ItemStack(Items.EMERALD),
					new ItemStack(FossilFindersModItems.FOSSIL_ARMOR_LEGGINGS.get()), 10, 5, 0.05f));
			event.getTrades().get(1).add(new BasicItemListing(new ItemStack(FossilFindersModBlocks.DESERT_FOSSIL.get()), new ItemStack(Items.EMERALD),
					new ItemStack(FossilFindersModItems.FOSSIL_ARMOR_BOOTS.get()), 10, 5, 0.05f));
		}
		if (event.getType() == VillagerProfession.MASON) {
			event.getTrades().get(1).add(new BasicItemListing(new ItemStack(FossilFindersModBlocks.DESERT_FOSSIL.get(), 8),
					new ItemStack(Items.EMERALD), new ItemStack(FossilFindersModItems.STURDY_FOSSIL.get(), 6), 10, 5, 0.05f));
		}
	}
}
