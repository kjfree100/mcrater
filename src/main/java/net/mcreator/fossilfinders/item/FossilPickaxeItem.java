
package net.mcreator.fossilfinders.item;

import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.Tier;
import net.minecraft.world.item.PickaxeItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;

import net.mcreator.fossilfinders.init.FossilFindersModTabs;
import net.mcreator.fossilfinders.init.FossilFindersModItems;

public class FossilPickaxeItem extends PickaxeItem {
	public FossilPickaxeItem() {
		super(new Tier() {
			public int getUses() {
				return 1164;
			}

			public float getSpeed() {
				return 12f;
			}

			public float getAttackDamageBonus() {
				return 4f;
			}

			public int getLevel() {
				return 6;
			}

			public int getEnchantmentValue() {
				return 42;
			}

			public Ingredient getRepairIngredient() {
				return Ingredient.of(new ItemStack(FossilFindersModItems.STURDY_FOSSIL.get()));
			}
		}, 1, -3f, new Item.Properties().tab(FossilFindersModTabs.TAB_DESERT_FOSSILS_MOD));
	}
}
