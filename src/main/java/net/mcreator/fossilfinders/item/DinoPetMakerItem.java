
package net.mcreator.fossilfinders.item;

import net.minecraft.world.item.context.UseOnContext;
import net.minecraft.world.item.UseAnim;
import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Item;
import net.minecraft.world.InteractionResult;

import net.mcreator.fossilfinders.procedures.DinoPetMakerRightclickedOnBlockProcedure;
import net.mcreator.fossilfinders.init.FossilFindersModTabs;

public class DinoPetMakerItem extends Item {
	public DinoPetMakerItem() {
		super(new Item.Properties().tab(FossilFindersModTabs.TAB_DESERT_FOSSILS_MOD).stacksTo(64).rarity(Rarity.COMMON));
	}

	@Override
	public UseAnim getUseAnimation(ItemStack itemstack) {
		return UseAnim.BLOCK;
	}

	@Override
	public InteractionResult useOn(UseOnContext context) {
		InteractionResult retval = super.useOn(context);
		DinoPetMakerRightclickedOnBlockProcedure.execute(context.getLevel(), context.getClickedPos().getX(), context.getClickedPos().getY(),
				context.getClickedPos().getZ(), context.getItemInHand());
		return retval;
	}
}
