
package net.mcreator.fossilfinders.item;

import net.minecraft.world.item.Rarity;
import net.minecraft.world.item.Item;

import net.mcreator.fossilfinders.init.FossilFindersModTabs;

public class SturdyFossilItem extends Item {
	public SturdyFossilItem() {
		super(new Item.Properties().tab(FossilFindersModTabs.TAB_DESERT_FOSSILS_MOD).stacksTo(64).rarity(Rarity.UNCOMMON));
	}
}
