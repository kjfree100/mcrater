
package net.mcreator.fossilfinders.item;

import net.minecraft.world.entity.ai.attributes.Attributes;
import javax.annotation.Nullable;

public class AncientCharmItem extends Item {

	public AncientCharmItem() {
		super(new Item.Properties().tab(FossilFindersModTabs.TAB_DESERT_FOSSILS_MOD).durability(135).rarity(Rarity.RARE));
	}

	@Override
	public InteractionResultHolder<ItemStack> use(Level world, Player entity, InteractionHand hand) {
		InteractionResultHolder<ItemStack> ar = super.use(world, entity, hand);
		ItemStack itemstack = ar.getObject();
		double x = entity.getX();
		double y = entity.getY();
		double z = entity.getZ();

		AncientCharmItemInHandTickProcedure.execute();
		return ar;
	}

}
