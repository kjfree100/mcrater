
package net.mcreator.fossilfinders.client.renderer;

import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.entity.EntityRendererProvider;

import net.mcreator.fossilfinders.entity.DinoPetEntity;
import net.mcreator.fossilfinders.client.model.Modelcustom_model;

public class DinoPetRenderer extends MobRenderer<DinoPetEntity, Modelcustom_model<DinoPetEntity>> {
	public DinoPetRenderer(EntityRendererProvider.Context context) {
		super(context, new Modelcustom_model(context.bakeLayer(Modelcustom_model.LAYER_LOCATION)), 0.5f);
	}

	@Override
	public ResourceLocation getTextureLocation(DinoPetEntity entity) {
		return new ResourceLocation("fossil_finders:textures/entities/texture.png");
	}
}
