package net.mcreator.fossilfinders.client.model;

import net.minecraft.world.entity.Entity;
import net.minecraft.util.Mth;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.CubeDeformation;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.EntityModel;

import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.blaze3d.vertex.PoseStack;

// Made with Blockbench 4.3.1
// Exported for Minecraft version 1.17 - 1.18 with Mojang mappings
// Paste this class into your mod and generate all required imports
public class Modelcustom_model<T extends Entity> extends EntityModel<T> {
	// This layer location should be baked with EntityRendererProvider.Context in
	// the entity renderer and passed into this model's constructor
	public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(new ResourceLocation("fossil_finders", "modelcustom_model"),
			"main");
	public final ModelPart Head;
	public final ModelPart Tail;
	public final ModelPart Tail2;
	public final ModelPart Tail3;
	public final ModelPart LeftLeg;
	public final ModelPart RightLeg;
	public final ModelPart Body;

	public Modelcustom_model(ModelPart root) {
		this.Head = root.getChild("Head");
		this.Tail = root.getChild("Tail");
		this.Tail2 = root.getChild("Tail2");
		this.Tail3 = root.getChild("Tail3");
		this.LeftLeg = root.getChild("LeftLeg");
		this.RightLeg = root.getChild("RightLeg");
		this.Body = root.getChild("Body");
	}

	public static LayerDefinition createBodyLayer() {
		MeshDefinition meshdefinition = new MeshDefinition();
		PartDefinition partdefinition = meshdefinition.getRoot();
		PartDefinition Head = partdefinition.addOrReplaceChild("Head",
				CubeListBuilder.create().texOffs(0, 20).addBox(-3.0F, -2.9F, -8.9F, 6.0F, 4.0F, 9.0F, new CubeDeformation(0.0F)).texOffs(6, 9)
						.addBox(-3.5F, -2.0F, -2.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(0, 7)
						.addBox(1.5F, -2.0F, -2.5F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)),
				PartPose.offset(0.0F, 5.0F, -10.0F));
		PartDefinition cube_r1 = Head.addOrReplaceChild("cube_r1",
				CubeListBuilder.create().texOffs(6, 7).addBox(-0.55F, 2.5F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(10, 7)
						.addBox(0.75F, 2.5F, -8.0F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(25, 26).addBox(-1.75F, 2.5F, -8.0F, 1.0F,
								1.0F, 1.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 1.0F, 0.0F, -0.1309F, 0.0F, 0.0F));
		PartDefinition cube_r2 = Head.addOrReplaceChild("cube_r2",
				CubeListBuilder.create().texOffs(30, 22).addBox(-2.7F, 3.3F, -6.9F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(29, 20)
						.addBox(-2.7F, 1.8F, -4.6F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(4, 27)
						.addBox(-2.7F, 2.5F, -5.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(32, 0)
						.addBox(1.7F, 1.8F, -4.6F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(32, 3)
						.addBox(1.7F, 2.5F, -5.8F, 1.0F, 1.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(32, 6).addBox(1.7F, 3.3F, -6.9F, 1.0F, 1.0F,
								1.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 1.0F, 0.0F, -0.3054F, 0.0F, 0.0F));
		PartDefinition cube_r3 = Head.addOrReplaceChild("cube_r3",
				CubeListBuilder.create().texOffs(28, 0).addBox(-3.0F, -20.4F, -13.9F, 6.0F, 2.0F, 9.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 19.0F, 10.0F, 0.2618F, 0.0F, 0.0F));
		PartDefinition Tail = partdefinition.addOrReplaceChild("Tail", CubeListBuilder.create(), PartPose.offset(2.5F, 11.0F, 8.8F));
		PartDefinition cube_r4 = Tail.addOrReplaceChild("cube_r4",
				CubeListBuilder.create().texOffs(0, 33).addBox(-2.5F, -15.8F, 6.0F, 5.0F, 5.0F, 8.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(-2.5F, 13.0F, -8.8F, -0.1745F, 0.0F, 0.0F));
		PartDefinition Tail2 = partdefinition.addOrReplaceChild("Tail2", CubeListBuilder.create(), PartPose.offset(2.0F, 13.3F, 16.1F));
		PartDefinition cube_r5 = Tail2.addOrReplaceChild("cube_r5",
				CubeListBuilder.create().texOffs(36, 14).addBox(-2.0F, -13.5635F, 4.1519F, 4.0F, 4.0F, 6.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(-2.0F, 10.7F, -6.1F, -0.1745F, 0.0F, 0.0F));
		PartDefinition Tail3 = partdefinition.addOrReplaceChild("Tail3", CubeListBuilder.create(), PartPose.offset(1.5F, 14.2F, 22.1F));
		PartDefinition cube_r6 = Tail3.addOrReplaceChild("cube_r6",
				CubeListBuilder.create().texOffs(0, 11).addBox(-0.5F, -12.0635F, 16.1519F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(0, 20)
						.addBox(-1.0F, -12.5635F, 14.1519F, 2.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(0, 0)
						.addBox(-1.5F, -13.0635F, 10.1519F, 3.0F, 3.0F, 4.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(-1.5F, 9.8F, -12.1F, -0.1745F, 0.0F, 0.0F));
		PartDefinition LeftLeg = partdefinition.addOrReplaceChild("LeftLeg",
				CubeListBuilder.create().texOffs(28, 6).addBox(2.0F, 10.3F, -4.3F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(28, 3)
						.addBox(0.0F, 10.3F, -4.3F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(28, 0)
						.addBox(-2.0F, 10.3F, -4.3F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)),
				PartPose.offset(3.5F, 12.7F, 1.3F));
		PartDefinition cube_r7 = LeftLeg.addOrReplaceChild("cube_r7",
				CubeListBuilder.create().texOffs(49, 0).addBox(-1.5F, -10.0F, 5.1F, 3.0F, 6.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(0, 46)
						.addBox(-3.0F, -2.0F, 1.0F, 6.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.5F, 11.3F, -1.3F, 0.4363F, 0.0F, 0.0F));
		PartDefinition cube_r8 = LeftLeg.addOrReplaceChild("cube_r8",
				CubeListBuilder.create().texOffs(26, 49).addBox(-1.5F, -8.0F, -0.5F, 3.0F, 6.0F, 2.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.5F, 11.3F, -1.3F, -0.4363F, 0.0F, 0.0F));
		PartDefinition cube_r9 = LeftLeg.addOrReplaceChild("cube_r9",
				CubeListBuilder.create().texOffs(42, 42).addBox(-3.0F, -2.1F, -1.0F, 6.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.5F, 11.3F, -1.3F, 0.2618F, 0.0F, 0.0F));
		PartDefinition RightLeg = partdefinition.addOrReplaceChild("RightLeg",
				CubeListBuilder.create().texOffs(23, 23).addBox(1.0F, 10.3F, -4.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(25, 20)
						.addBox(-1.0F, 10.3F, -4.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(21, 26)
						.addBox(-3.0F, 10.3F, -4.0F, 1.0F, 1.0F, 2.0F, new CubeDeformation(0.0F)),
				PartPose.offset(-3.5F, 12.7F, 1.0F));
		PartDefinition cube_r10 = RightLeg.addOrReplaceChild("cube_r10",
				CubeListBuilder.create().texOffs(38, 38).addBox(-11.0F, -2.1F, -1.0F, 6.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(7.5F, 11.3F, -1.0F, 0.2618F, 0.0F, 0.0F));
		PartDefinition cube_r11 = RightLeg.addOrReplaceChild("cube_r11",
				CubeListBuilder.create().texOffs(16, 47).addBox(-1.5F, -10.0F, 5.1F, 3.0F, 6.0F, 2.0F, new CubeDeformation(0.0F)).texOffs(24, 45)
						.addBox(-3.0F, -2.0F, 1.0F, 6.0F, 2.0F, 2.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(-0.5F, 11.3F, -1.0F, 0.4363F, 0.0F, 0.0F));
		PartDefinition cube_r12 = RightLeg.addOrReplaceChild("cube_r12",
				CubeListBuilder.create().texOffs(40, 46).addBox(-1.5F, -8.0F, -0.5F, 3.0F, 6.0F, 2.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(-0.5F, 11.3F, -1.0F, -0.4363F, 0.0F, 0.0F));
		PartDefinition Body = partdefinition.addOrReplaceChild(
				"Body", CubeListBuilder.create().texOffs(40, 24).addBox(1.5F, -13.4F, 0.0F, 4.0F, 3.0F, 4.0F, new CubeDeformation(0.0F))
						.texOffs(26, 38).addBox(-5.5F, -13.4F, 0.0F, 4.0F, 3.0F, 4.0F, new CubeDeformation(0.0F)),
				PartPose.offset(0.0F, 24.0F, 0.0F));
		PartDefinition cube_r13 = Body.addOrReplaceChild("cube_r13",
				CubeListBuilder.create().texOffs(21, 20).addBox(2.0F, -8.0F, -10.7F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(0, 24)
						.addBox(7.0F, -8.0F, -10.7F, 1.0F, 4.0F, 1.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(-5.0F, 0.0F, 0.0F, -0.5236F, 0.0F, 0.0F));
		PartDefinition cube_r14 = Body.addOrReplaceChild("cube_r14",
				CubeListBuilder.create().texOffs(10, 0).addBox(2.0F, 10.0F, -5.9F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(28, 11)
						.addBox(7.0F, 10.0F, -5.9F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(-5.0F, 0.0F, 0.0F, -2.0071F, 0.0F, 0.0F));
		PartDefinition cube_r15 = Body.addOrReplaceChild("cube_r15",
				CubeListBuilder.create().texOffs(0, 0).addBox(2.0F, -2.6F, -13.1F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)).texOffs(4, 24)
						.addBox(7.0F, -2.6F, -13.1F, 1.0F, 2.0F, 1.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(-5.0F, 0.0F, 0.0F, -0.6981F, 0.0F, 0.0F));
		PartDefinition cube_r16 = Body.addOrReplaceChild("cube_r16",
				CubeListBuilder.create().texOffs(21, 24).addBox(-2.5F, -13.0F, -19.0F, 5.0F, 5.0F, 9.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.48F, 0.0F, 0.0F));
		PartDefinition cube_r17 = Body.addOrReplaceChild("cube_r17",
				CubeListBuilder.create().texOffs(0, 0).addBox(-3.5F, -16.0F, -7.0F, 7.0F, 6.0F, 14.0F, new CubeDeformation(0.0F)),
				PartPose.offsetAndRotation(0.0F, 0.0F, 0.0F, -0.1309F, 0.0F, 0.0F));
		return LayerDefinition.create(meshdefinition, 64, 64);
	}

	@Override
	public void renderToBuffer(PoseStack poseStack, VertexConsumer vertexConsumer, int packedLight, int packedOverlay, float red, float green,
			float blue, float alpha) {
		Head.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		Tail.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		Tail2.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		Tail3.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		LeftLeg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		RightLeg.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
		Body.render(poseStack, vertexConsumer, packedLight, packedOverlay, red, green, blue, alpha);
	}

	public void setupAnim(T entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch) {
		this.LeftLeg.xRot = Mth.cos(limbSwing * 1.0F) * -1.0F * limbSwingAmount;
		this.RightLeg.xRot = Mth.cos(limbSwing * 1.0F) * 1.0F * limbSwingAmount;
		this.Tail.yRot = Mth.cos(limbSwing * 1.0F) * 1.0F * limbSwingAmount;
		this.Tail3.yRot = Mth.cos(limbSwing * 1.0F) * 1.0F * limbSwingAmount;
		this.Tail2.yRot = Mth.cos(limbSwing * 0.6662F + (float) Math.PI) * limbSwingAmount;
	}
}
